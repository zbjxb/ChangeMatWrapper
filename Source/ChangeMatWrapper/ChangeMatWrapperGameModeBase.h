// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ChangeMatWrapperGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHANGEMATWRAPPER_API AChangeMatWrapperGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
