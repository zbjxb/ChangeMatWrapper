// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class ChangeMatWrapperEditorTarget : TargetRules
{
	public ChangeMatWrapperEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "ChangeMatWrapper" } );
	}
}
