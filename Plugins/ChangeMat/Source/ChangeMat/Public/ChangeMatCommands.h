// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "ChangeMatStyle.h"

class FChangeMatCommands : public TCommands<FChangeMatCommands>
{
public:

	FChangeMatCommands()
		: TCommands<FChangeMatCommands>(TEXT("ChangeMat"), NSLOCTEXT("Contexts", "ChangeMat", "ChangeMat Plugin"), NAME_None, FChangeMatStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};