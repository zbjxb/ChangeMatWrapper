#pragma once
#include "CoreMinimal.h"

#include "Widgets/SBoxPanel.h"
#include "Widgets/Input/SEditableTextBox.h"
#include "Widgets/Text/STextBlock.h"

#include "Input/Reply.h"
#include "Types/SlateEnums.h"
#include "Widgets/DeclarativeSyntaxSupport.h"

class SChangeMatWidget : public SCompoundWidget
{
public:
	DECLARE_DELEGATE_TwoParams( FOnOkButtonClicked, const FString&, const FString& );

	SLATE_BEGIN_ARGS( SChangeMatWidget  ) {}
	// OnOkButtonClicked的名字可以随便取
	SLATE_EVENT( FOnOkButtonClicked, OnOkButtonClicked )
		SLATE_END_ARGS( )

		void Construct( const FArguments& Args );

	void SrcPathSelected( const FString& Path );
	void DestPathSelected( const FString& Path );

	void ShowLog( const FString& LogFilePath );

private:

	// 名字可以随便取
	FReply OnClicked( );

	// OnExecute名字可以随便取
	FOnOkButtonClicked OnExecute;

	FString SrcPathPicked;
	FString DestPathPicked;

	TSharedPtr<SEditableText> LogWnd;
};
