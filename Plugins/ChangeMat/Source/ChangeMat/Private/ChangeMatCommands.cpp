// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ChangeMatCommands.h"

#define LOCTEXT_NAMESPACE "FChangeMatModule"

void FChangeMatCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "ChangeMat", "Bring up ChangeMat window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
